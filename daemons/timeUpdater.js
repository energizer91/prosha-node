/**
 * Created by energizer on 20.03.17.
 */
/**
 * Created by Алекс on 27.11.2016.
 */
var configs = require('../configs'),
    mongo = require('../helpers/mongo')(configs),
    CronJob = require('cron').CronJob,
    messages = {
        daily: {
            text: 'До ГОСов осталось (столько-то) дней, но у тебя впереди целый день, чтобы начать к ним готовиться!\nХорошего дня!'
        },
        nightly: {
            text: 'А ты готовился сегодня к ГОСам?',
            buttons: true
        }
    };

var broadcastUpdate =function (message, params) {
        return mongo.User.find({subscribed: true}).then(function (users) {
            users.forEach(function (user) {
                process.send({
                    type: 'message',
                    userId: user.user_id,
                    message: message,
                    params: params
                });
            })
        }).catch(function (error) {
            console.error(new Date(), 'An error occured: ' + error.message);
        }).done();
    };

new CronJob('0 0 9 * * *', function () {
    console.log('Starting morning broadcast');
    return broadcastUpdate(messages.daily);
});

new CronJob('0 0 22 * * *', function () {
    console.log('Starting morning broadcast');
    return broadcastUpdate(messages.nightly);
});

process.on('message', function(m) {
    console.log('CHILD got message:', m);
    if (m.type == 'service') {
        switch (m.action) {
            case 'message':
                mongo.User.findOne({user_id: m.value}).then(function (user) {
                    process.send({type: 'message', userId: user.user_id, message: 'Проверка'});
                });
                break;
            case 'broadcast':
                var message = '';
                broadcastUpdate(messages[m.value]);
                break;
            default:
                console.log('Unknown service command');
                break;
        }
    }
});

process.send({ message: 'ready' });