/**
 * Created by Александр on 13.12.2015.
 */
module.exports = function (express, botApi, configs) {
    var router = express.Router(),
        Q = require('q');

    var performStart = function (command, message, user) {
            if (!user.subscribed) {
                return botApi.mongo.User.findOneAndUpdate({user_id: user.user_id}, {subscribed: true}).then(function () {
                    return botApi.bot.sendMessage(message.chat.id, 'Я буду присылать тебе напоминания в 9:00 и 22:00');
                })
            }
            return botApi.bot.sendMessage(message.chat.id, 'Я существую чтобы мотивировать тебя.');
        },
        commands = {
            '/broadcast': function (command, message, user) {
                if (!user.admin) {
                    throw new Error('Unauthorized access');
                }

                if (command.length <= 1) {
                    return botApi.bot.sendMessage(message.chat.id, 'Отсутствует текст рассылки');
                }

                command.splice(0, 1);

                return botApi.mongo.User.find({subscribed: true/*user_id: {$in: [85231140, 5630968, 226612010]}*/}).then(function (users) {
                    return botApi.request.fulfillAll(users.map(function (user) {
                        return this.sendMessage(user.user_id, command.join(' '));
                    }, botApi.bot));
                }).finally(botApi.bot.sendMessage.bind(botApi.bot, message.chat.id, 'Рассылка окончена.'));

            },
            '/grant': function (command, message, user) {
                if (!user.admin) {
                    throw new Error('Unauthorized access');
                }

                if (command.length <= 1) {
                    return botApi.bot.sendMessage(message.chat.id, 'Введите id пользователя.');
                }

                var privileges = {};

                if (command[2]) {
                    if (command[2] == 'admin') {
                        privileges.admin = true;
                    } else if (command[2] == 'editor') {
                        privileges.editor = true;
                    }
                } else {
                    privileges.admin = false;
                    privileges.editor = false;
                }

                return botApi.mongo.User.findOneAndUpdate({user_id: parseInt(command[1])}, privileges).then(function () {
                    return botApi.bot.sendMessage(parseInt(command[1]), 'Вам были выданы привилегии администратора пользователем ' + user.first_name + '(' + user.username + ')');
                }).finally(botApi.bot.sendMessage.bind(botApi.bot, message.chat.id, 'Привилегии присвоены.'));
            },
            '/user': function (command, message, user) {
                if (command[1] == 'count') {
                    return botApi.mongo.User.count().then(function (count) {
                        return botApi.bot.sendMessage(message.chat.id, 'Всего пользователей на данный момент: ' + count);
                    })
                } else if (command[1] == 'subscribed') {
                    return botApi.mongo.User.find({subscribed: true}).count().then(function (count) {
                        return botApi.bot.sendMessage(message.chat.id, 'Подписанных пользователей на данный момент: ' + count);
                    })
                } else if (command[1] == 'id') {
                    return botApi.bot.sendMessage(message.chat.id, 'id текущего пользователя: ' + message.from.id);
                } else if (command[1]) {
                    return botApi.mongo.User.findOne({username: command[1]}).then(function (user) {
                        return botApi.bot.sendMessage(message.chat.id, sendUserInfo(user));
                    })
                }
                return botApi.bot.sendMessage(message.chat.id, {
                    text: sendUserInfo(user)
                }, {disableButtons: true, parse_mode: 'Markdown'});
            },
            '/start': performStart,
            '/help': performStart
        },
        sendUserInfo = function (user) {
            return '```\n' +
                'User ' + user.user_id + ':\n' +
                'Имя:        ' + (user.first_name    || 'Не указано') + '\n' +
                'Фамилия:    ' + (user.last_name     || 'Не указано') + '\n' +
                'Ник:        ' + (user.username      || 'Не указано') + '\n' +
                'Подписка:   ' + (user.subscribed     ? 'Подписан' : 'Не подписан') + '\n' +
                'Админ:      ' + (user.admin          ? 'Присвоен' : 'Не присвоен') + '\n' + '```'
        },
        performCommand = function (command, data, user) {
            return commands[command[0]].call(botApi.bot, command, data, user);
        },
        writeLog = function (data, result, error) {
            var logRecord = new botApi.mongo.Log({
                date: new Date(),
                request: data,
                response: result,
                error: error
            });

            return logRecord.save()
        },
        updateUser = function (user, callback) {
            if (!user) {
                return {};
            }
            return botApi.mongo.User.findOneAndUpdate({user_id: user.id}, user, {new: true, upsert: true}, callback);
        },
        updateKarma = function (userId, karma, params) {
            return botApi.mongo.User.findOneAndUpdate({user_id: userId}, {$inc: {karma: karma}}).then(function (updatedUser) {
                var message = '';
                if (karma > 0) {
                    message = 'Молодец! Твоя карма ' + (updatedUser.karma + karma) + '. Спокойной ночи';
                } else {
                    message = 'Всё равно молодец, ведь у тебя есть завтрашний день! Твоя карма ' + (updatedUser.karma + karma) + '. Спокойной ночи';
                }
                return botApi.bot.sendMessage(userId, message, params);
            });
        },
        performCallbackQuery = function (queryData, data, params) {
            if (!params) {
                params = {};
            }
            switch (queryData[0]) {
                case 'plus':
                    return botApi.bot.answerCallbackQuery(data.callback_query.id)
                        .finally(function () {
                            return updateKarma(data.callback_query.message.chat.id, +1, params);
                        });
                    break;
                case 'minus':
                    return botApi.bot.answerCallbackQuery(data.callback_query.id)
                        .finally(function () {
                            return updateKarma(data.callback_query.message.chat.id, -1, params);
                        });
                    break;
            }

            throw new Error('Unknown callback query ' + queryData);
        },
        performWebHook = function (data, response) {
            return Q.Promise(function (resolve, reject) {

                response.status(200);
                response.json({status: 'OK'});

                if (!data) {
                    return reject(new Error('No webhook data specified'));
                }

                var userObject = data.message || data.inline_query || data.callback_query;

                updateUser((userObject || {}).from, function (err, user) {
                    if (err) {
                        console.error(err);
                        return resolve({});
                    }
                    return resolve(user);
                });
            }).then(function (user) {
                if (data.hasOwnProperty('callback_query')) {
                    var queryData = data.callback_query.data.split(' ');
                    return performCallbackQuery(queryData, data, {language: user.language});
                } else if (data.message && data.message.text) {
                    var message = data.message;

                    var command = (message.text || '').split(' ');
                    if (command[0].indexOf('@') >= 0) {
                        command[0] = command[0].split('@')[0];
                    }

                    if (commands[command[0]]) {
                        return performCommand(command, message, user);
                    }

                    console.error('Unknown command', data);
                    throw new Error('Command not found: ' + command.join(' '));
                }

                console.error('unhandled message', data);
                throw new Error('No message specified');
            }).then(function (response) {
                return writeLog(data, response).then(function () {
                    return response;
                })
            }).catch(function (error) {
                console.error(error);
                return writeLog(data, {}, error).then(function () {
                    return error;
                })
            });
        };

    router.get('/', function (req, res) {
        return res.send('hello fot Telegram bot api');
    });

    router.get('/getMe', function (req, res, next) {
        return botApi.bot.getMe().then(function (response) {
            return res.send(JSON.stringify(response));
        }).catch(next);
    });

    router.route('/webhook')
        .post(function (req, res) {
            return performWebHook(req.body, res);
        });

    return {
        endPoint: '/bot',
        router: router
    };
};