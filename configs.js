/**
 * Created by Алекс on 29.11.2016.
 */

module.exports = {
    mongo: require('./config/mongodb'),
    bot: require('./config/telegram')
};