/**
 * Created by Александр on 16.04.2016.
 */

module.exports = function (configs) {
    var botConfig = configs.bot,
        requestHelper = require('./request')(configs),
        Queue = require('promise-queue'),
        q = require('q'),
        botMethods = {
            sendRequest: function (request, params, method) {
                var botUrl = botConfig.url + botConfig.token + '/' + request,
                    parameters = requestHelper.prepareConfig(botUrl, method);

                return requestHelper.makeRequest(parameters, params);
            },
            answerCallbackQuery: function (queryId, load) {
                if (!load) {
                    load = {};
                }
                return this.sendRequest('answerCallbackQuery', {
                    callback_query_id: queryId,
                    text: load.text,
                    show_alert: load.show_alert,
                    url: load.url
                }).catch(function (error) {
                    console.error(error);
                    return {};
                });
            },
            sendChatAction: function (userId, action) {
                return this.sendRequest('sendChatAction', {
                    chat_id: userId,
                    action: action
                });
            },
            prepareButtons: function (message, params) {
                var buttons = [];

                if (!params) {
                    params = {};
                }

                if (params.buttons) {
                    buttons = params.buttons;
                } else if (!params.disableButtons) {
                    // add buttons here
                    if (message.buttons && params) {
                        buttons.push([]);
                        buttons[buttons.length - 1].push({
                            text: '+',
                            callback_data: 'plus'
                        });
                        buttons[buttons.length - 1].push({
                            text: '-',
                            callback_data: 'minus'
                        });
                    }
                }

                return buttons;
            },
            editMessageButtons: function (message, buttons) {
                if (!message) {
                    return;
                }

                if (buttons) {
                    message.reply_markup = JSON.stringify({inline_keyboard: buttons});
                } else {
                    var newButtons = this.prepareButtons(message);

                    message.reply_markup = JSON.stringify({inline_keyboard: newButtons});
                }

                if (message.chat && message.chat.id) {
                    message.chat_id = message.chat.id;
                }

                message.text = 'Решение принято';

                return this.sendRequest('editMessageReplyMarkup', message).then(function (response) {
                    return response;
                }).catch(function (error) {
                    console.error('Editing message error', error);
                    return {};
                });
            },
            editMessage: function (message) {
                if (!message) {
                    return;
                }

                message.reply_markup = this.prepareButtons(message);

                return this.sendRequest('editMessageText', message);
            },
            sendMessage: function (userId, message, params) {
                if (!message) {
                    return;
                }

                if (!params) {
                    params = {};
                }

                var sendMessage,
                    buttons = [];

                if (typeof message == 'string') {
                    sendMessage = {
                        chat_id: userId,
                        text: message
                    };
                } else {
                    buttons = this.prepareButtons(message, params);

                    sendMessage = {
                        chat_id: userId,
                        text: message.text + ((message.attachments && message.attachments.length > 0) ? '\n(Вложений: ' + message.attachments.length + ')' : '')
                    };

                    if (params.parse_mode) {
                        sendMessage.parse_mode = params.parse_mode;
                    }
                }

                if (buttons.length > 0) {
                    sendMessage.reply_markup = {};
                    sendMessage.reply_markup.inline_keyboard = buttons;
                    sendMessage.reply_markup = JSON.stringify(sendMessage.reply_markup);
                }

                if (message.reply_to_message_id) {
                    sendMessage.reply_to_message_id = message.reply_to_message_id;
                }

                return this.sendRequest('sendMessage', sendMessage);
            },
            sendMessages: function (userId, messages, params) {
                var messageQueue = new Queue(1, Infinity);
                return (messages || []).reduce(function (p, message) {
                    return p.then(messageQueue.add.bind(messageQueue, this.sendMessage.bind(this, userId, message, params)));
                }.bind(this), q.when());
            },
            sendMessageToAdmin: function (text) {
                return this.sendMessage(botConfig.adminChat, text);
            },
            forwardMessage: function (userId, message, params) {
                var buttons = this.prepareButtons(message, params),
                    sendMessage = {
                        chat_id: userId,
                        text: message.text,
                        caption: message.caption
                    },
                    commandType = '';

                if (buttons.length) {
                    sendMessage.reply_markup = {};
                    sendMessage.reply_markup.inline_keyboard = buttons;
                    sendMessage.reply_markup = JSON.stringify(sendMessage.reply_markup);
                }

                if (params.native) {
                    var chatId = userId;

                    if (message && message.chat && message.chat.id) {
                        chatId = message.chat.id;
                    }

                    commandType = 'forwardMessage';
                    sendMessage = {
                        chat_id: userId,
                        from_chat_id: chatId,
                        message_id: message.message_id
                    }
                } else if (message.audio && message.audio.file_id) {
                    commandType = 'sendAudio';
                    sendMessage.audio = message.audio.file_id;
                } else if (message.voice && message.voice.file_id) {
                    commandType = 'sendVoice';
                    sendMessage.voice = message.voice.file_id;
                } else if (message.document && message.document.file_id) {
                    commandType = 'sendDocument';
                    sendMessage.document = message.document.file_id;
                } else if (message.photo && message.photo.length > 0) {
                    commandType = 'sendPhoto';
                    sendMessage.photo = message.photo[message.photo.length - 1].file_id;
                } else {
                    commandType = 'sendMessage';
                    sendMessage.text = sendMessage.text || 'Пустое сообщение';
                }

                return this.sendRequest(commandType, sendMessage);
            },
            forwardMessages: function (userId, messages, params) {
                var messageQueue = new Queue(1, Infinity);
                return (messages || []).reduce(function (p, message) {
                    return p.then(messageQueue.add.bind(messageQueue, this.forwardMessage.bind(this, userId, message, params)));
                }.bind(this), q.when());
            },
            getMe: function () {
                return this.sendRequest('getMe');
            }
        };

    Queue.configure(require('q').Promise);

    return botMethods;

};