/**
 * Created by xgmv84 on 11/26/2016.
 */

module.exports = function (configs) {
    var mongoose = require('mongoose'),
        config = configs.mongo,
        db = mongoose.connection;

    mongoose.Promise = require('q').Promise;

    db.on('error', console.error);

    db.once('open', function () {
        console.log('MongoDB connection successful');
    });

    var userSchema = mongoose.Schema({
            username: String,
            first_name: String,
            last_name: String,
            user_id: Number,
            subscribed: {type: Boolean, default: false},
            admin: {type: Boolean, default: false},
            karma: {type: Number, default: 0}
        }),
        logSchema = mongoose.Schema({
            date: Date,
            request: Object,
            response: Object,
            error: Object
        });

    mongoose.connect('mongodb://' + config.server + '/' + config.database);

    logSchema.index({date: 1}, {expireAfterSeconds: 60 * 60 * 24 * 7});


    return {
        User: mongoose.model('User', userSchema),
        Log: mongoose.model('Log', logSchema)
    };
};